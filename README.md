# CS22 Submissions: Enero 2018

Para hacer una entrega en este curso:

1. En Bitbucket, hacer un fork personal de este repositorio.

2. Hacer un Pull en un directorio local. (e.g. usando SourceTree.)

3. Crear un submission.

Los submissions copias de el `bitbucket-pipelines.yml`.

Si soy `viktor32` en Bitbucket, haciendo un submission con tag `ejemplo_0`, mi directorio se vería así:

```

    - viktor32
      - ejemplo_0
        - bitbucket-pipelines.yml

```

(Hay un script muy primitivo que se llama `bin/create_submission` que puedes utilizar para esto también.)
